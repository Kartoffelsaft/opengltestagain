#include <cstdio>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "./program.hpp"
#include "../shaders/shaderLoader.hpp"

void errorHandle(int code, const char* description)
{
    fprintf(stderr, "glfw error code %i: %s\n", code, description);
}

Program::Program()
{
    glewExperimental = true;

    if(!glfwInit())
    { fprintf(stderr, "glfw done broke (initialization)\n"); }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

//    glfwSetErrorCallback(errorHandle);

    window = glfwCreateWindow(resX, resY, "openGL tutorial again", nullptr, nullptr);

    if(!window)
    {
        fprintf(stderr, "glfw done broke (window creation)\n");

        glfwTerminate();
    }

    glfwMakeContextCurrent(window);
    glewExperimental=true;
    if(glewInit() != GLEW_OK)
    {
        fprintf(stderr, "GLEW done broke (initialization)\n");

        glfwTerminate();
    }

    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);



    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    shaderID = compileShaders();

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

    vertexBufferData = {
        -1, -1, 0,
        1, -1, 0,
        0, 1, 0
    };

    glBufferData(GL_ARRAY_BUFFER, vertexBufferData.size()* sizeof(GLfloat), vertexBufferData.data(), GL_STATIC_DRAW);
}

void Program::loop()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(shaderID);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDisableVertexAttribArray(0);

    glfwSwapBuffers(window);

    parseInput();
}

bool Program::checkLoop()
{ return !glfwWindowShouldClose(window); }

Program::~Program()
{
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteProgram(shaderID);

    glfwTerminate();
}

void Program::parseInput()
{
    glfwPollEvents();

    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {glfwSetWindowShouldClose(window, GL_TRUE);}
}


