#ifndef OPENGLTESTAGAIN_PROGRAM_HPP
#define OPENGLTESTAGAIN_PROGRAM_HPP

#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

struct Program
{
    GLFWwindow* window;

    GLuint shaderID;
    GLuint vertexArrayID;
    GLuint vertexBuffer;
    std::vector<GLfloat> vertexBufferData;

    static int const resX=800;
    static int const resY=600;

    void parseInput();

public:
    Program();

    void loop();
    bool checkLoop();

    ~Program();
};

#endif
