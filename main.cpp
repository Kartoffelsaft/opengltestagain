#include "./program/program.hpp"

int main()
{
    Program p;

    do
    {p.loop();}
    while(p.checkLoop());

    return 0;
}