#ifndef OPENGLTESTAGAIN_SHADERLOADER_HPP
#define OPENGLTESTAGAIN_SHADERLOADER_HPP

#include <GL/glew.h>

GLuint compileShaders();
GLuint compileShader(const char* directory, GLenum type);

#endif //OPENGLTESTAGAIN_SHADERLOADER_HPP
