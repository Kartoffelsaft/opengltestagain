#version 330 core

layout(location = 0) in vec3 v3Pos;

void main()
{
    gl_Position.xyz = v3Pos;
    gl_Position.w = 1;
}