#include <fstream>
#include <string>
#include <cstring>

#include "shaderLoader.hpp"

void printPossibleShaderError(GLuint ID)
{
    GLint result = GL_FALSE;

    glGetShaderiv(ID, GL_COMPILE_STATUS, &result);
    if(!result)
    {
        int errorMessageLength;

        glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &errorMessageLength);

        char shaderInfoLog[errorMessageLength + 1];
        glGetShaderInfoLog(ID, errorMessageLength, nullptr, shaderInfoLog);
        fprintf(stderr, "shader %i compilation error: %s\n", ID, shaderInfoLog);
    }
}

void printPossibleFinalShaderError(GLuint ID)
{
    GLint result = GL_FALSE;

    glGetProgramiv(ID, GL_LINK_STATUS, &result);
    if(!result)
    {
        int errorMessageLength;

        glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &errorMessageLength);

        char shaderInfoLog[errorMessageLength + 1];
        glGetProgramInfoLog(ID, errorMessageLength, nullptr, shaderInfoLog);
        fprintf(stderr, "program %i compilation error: %s\n", ID, shaderInfoLog);
    }
}

GLuint compileShaders()
{
    GLuint vertShader = compileShader("./shaders/vertexShader.vert", GL_VERTEX_SHADER);
    printPossibleShaderError(vertShader);
    GLuint fragShader = compileShader("./shaders/fragmentShader.frag", GL_FRAGMENT_SHADER);
    printPossibleShaderError(fragShader);
    GLuint finalShader = glCreateProgram();

    glAttachShader(finalShader, vertShader);
    glAttachShader(finalShader, fragShader);
    glLinkProgram(finalShader);
    printPossibleFinalShaderError(finalShader);

    glDetachShader(finalShader, vertShader);
    glDetachShader(finalShader, fragShader);

    glDeleteShader(vertShader);
    glDeleteShader(fragShader);

    return finalShader;
}

GLuint compileShader(const char* const directory, GLenum type)
{
    std::ifstream file{directory};

    if(!file.is_open())
    {fprintf(stderr, "file %s could not be read", directory);}

    std::string source{std::istreambuf_iterator<char>(file),
                       std::istreambuf_iterator<char>()};
    GLuint shader = glCreateShader(type);

    const char* const cSource = source.c_str();
    glShaderSource(shader, 1, &cSource, nullptr);
    glCompileShader(shader);
    return shader;
}